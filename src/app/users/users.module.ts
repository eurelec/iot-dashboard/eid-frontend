import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailComponent } from './component/user-detail/user-detail.component';
import { UsersListComponent } from './component/users-list/users-list.component';
import { UsersRoutingModule } from '@app/users/users-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { FlexModule } from '@angular/flex-layout';

@NgModule({
  declarations: [UserDetailComponent, UsersListComponent],
  imports: [CommonModule, UsersRoutingModule, TranslateModule, MaterialModule, FlexModule],
})
export class UsersModule {}
