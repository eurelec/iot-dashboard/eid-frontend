import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  url = environment.serverUrl + '/Users';
  constructor(http: HttpClient) {}
}
