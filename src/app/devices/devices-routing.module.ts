import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

import { Shell } from '@app/shell/shell.service';
import { UsersListComponent } from '@app/users/component/users-list/users-list.component';
import { DevicesListComponent } from '@app/devices/component/devices-list/devices-list.component';
import { DeviceDetailComponent } from '@app/devices/component/device-detail/device-detail.component';

const routes: Routes = [
  Shell.childRoutes([
    { path: 'devices', component: DevicesListComponent, data: { title: marker('Devices') } },
    { path: 'devices/:id', component: DeviceDetailComponent, data: { title: marker('Device Details') } },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class DevicesRoutingModule {}
