import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Device } from '@app/devices/model/device.model';
import { DeviceStatus } from '@app/devices/model/device-status.model';

@Injectable({
  providedIn: 'root',
})
export class DeviceService {
  url = environment.serverUrl + '/devices';

  constructor(private http: HttpClient) {}

  getDevices(): Observable<Device[]> {
    return this.http.get<Device[]>(this.url);
  }

  getDevice(id: string): Observable<Device> {
    return this.http.get<Device>(this.url + '/' + id);
  }

  getDeviceStatus(id: string): Observable<DeviceStatus> {
    return this.http.get<DeviceStatus>(this.url + '/status/' + id);
  }
  setDeviceStatus(status: DeviceStatus): Observable<DeviceStatus> {
    return this.http.put<DeviceStatus>(this.url + '/status', status);
  }
}
