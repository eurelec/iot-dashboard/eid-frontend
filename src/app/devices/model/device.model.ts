import { DeviceType } from '@app/devices/model/device-type.model';
import { User } from '@app/users/model/user.model';

export interface Device {
  id: string;
  name: string;
  description: string;
  secret: string;
  owner: User;
  type: DeviceType;
}
