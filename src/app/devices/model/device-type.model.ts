export interface DeviceType {
  id: string;
  name: string;
}
