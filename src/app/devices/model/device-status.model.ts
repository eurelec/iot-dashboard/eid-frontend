export interface DeviceStatus {
  deviceId: string;
  isOn: boolean;
}
