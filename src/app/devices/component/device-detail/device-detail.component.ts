import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Device } from '@app/devices/model/device.model';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { DeviceService } from '@app/devices/service/device.service';
import { DeviceStatus } from '@app/devices/model/device-status.model';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.scss'],
})
export class DeviceDetailComponent implements OnInit {
  deviceForm: FormGroup = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    description: new FormControl(''),
    secret: new FormControl(''),
    owner: new FormControl(null),
    type: new FormControl(null),
  });
  deviceStatus: DeviceStatus = { deviceId: '', isOn: false };

  constructor(private activatedRoute: ActivatedRoute, private deviceService: DeviceService) {
    this.activatedRoute.paramMap.subscribe({
      next: (req) => {
        if (req.has('id')) {
          const id = req.get('id') ?? '';
          this.deviceService.getDevice(id).subscribe({
            next: (device: Device) => {
              this.deviceForm.setValue(device);
            },
          });
          this.deviceService.getDeviceStatus(id).subscribe({
            next: (status: DeviceStatus) => {
              this.deviceStatus = status;
            },
          });
        }
      },
    });
  }

  ngOnInit(): void {}

  onStatusChange(e: MatSlideToggleChange) {
    this.deviceStatus.isOn = e.checked;
    this.deviceService.setDeviceStatus(this.deviceStatus).subscribe({
      next: (status: DeviceStatus) => {
        this.deviceStatus = status;
      },
    });
  }
}
