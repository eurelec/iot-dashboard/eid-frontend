import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DevicesListComponent } from './component/devices-list/devices-list.component';
import { DeviceDetailComponent } from './component/device-detail/device-detail.component';
import { DevicesRoutingModule } from '@app/devices/devices-routing.module';
import { FlexModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DevicesListComponent, DeviceDetailComponent],
  imports: [
    CommonModule,
    FlexModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    TranslateModule,
    DevicesRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
})
export class DevicesModule {}
